package com.software.springbootoauthsocial.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {

    @Value("${spring.demo.text}")
    private String demo;

    @GetMapping
    public ResponseEntity<?> display() {
        return ResponseEntity.ok(demo);
    }
}
