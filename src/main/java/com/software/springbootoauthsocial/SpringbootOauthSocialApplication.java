package com.software.springbootoauthsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootOauthSocialApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootOauthSocialApplication.class, args);
    }

}
